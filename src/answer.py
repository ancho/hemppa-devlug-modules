import re

from nio import RoomMessageText

from modules.common.module import BotModule


class MatrixModule(BotModule):

    def __init__(self, name):
        self.qa = {
            r"[Ii]st es schon fertig\?": "Nö",
            r"[Ww]ann ist es denn fertig\?": "Wenn es fertig ist!",
            r"[Ww][Tt][Ff].*": "¯\\_(ツ)_/¯"
        }
        super().__init__(name)
        self.bot = None

    async def text_cb(self, room, event):
        for f in self.qa.keys():
            if re.findall(f, event.body):
                await self.bot.send_text(room, self.qa[f])

        if re.findall(r"[Yy][Ee][Aa][Hh]", event.body):
                await self.send_reaction(room, event.event_id, "🤟")

    async def send_reaction(self, room, event_id, emoji):
        msg = {
            "m.relates_to": {
                "rel_type": "m.annotation",
                "event_id": event_id,
                "key": emoji
            }
        }

        await self.bot.client.room_send(room.room_id, 'm.reaction', msg)


    async def matrix_message(self, bot, room, event):
        pass

    def matrix_start(self, bot):
        """
        Register callback for all RoomMessageText events on startup
        """
        super().matrix_start(bot)
        self.bot = bot
        bot.client.add_event_callback(self.text_cb, RoomMessageText)

    def matrix_stop(self, bot):
        super().matrix_stop(bot)
        bot.remove_callback(self.text_cb)


    def help(self):
        return 'Echoes back what user has said'
