import itertools
import random
import string
from typing import List, Tuple

from bot import Bot
from modules.common.module import BotModule
from nio import RoomCreateResponse, RoomCreateError, JoinedMembersResponse, JoinedMembersError, RoomMember, MatrixRoom, \
    RoomPreset


class MatrixModule(BotModule):

    def __init__(self, name):
        super().__init__(name)
        self.rooms = dict()
        self.jitsi_host = "https://meet.jit.si"
        self.version = "0.0.3"

    async def matrix_message(self, bot: Bot, room, event):
        args = event.body.split()

        if len(args) == 2:
            if args[1] == 'reset-rooms':
                bot.must_be_admin(room, event)
                self.rooms = dict()
            elif args[1] == 'list-rooms':
                bot.must_be_admin(room, event)                
                await bot.send_text(room, self.get_known_user_rooms_msg())
                return

        groups = await self.get_groups(bot, room)
        await self.send_jitsi_links(bot, room, groups)

    def set_settings(self, data):
        super().set_settings(data)
        if data.get("rooms") is not None:
            self.rooms = data.get("rooms")
            self.logger.debug(self.get_known_user_rooms_msg())
        if data.get("jitsi_host") is not None:
            self.jitsi_host = data.get("jitsi_host")
            self.logger.debug("using jitsi host: " + self.jitsi_host)

    def get_known_user_rooms_msg(self):
        msg = "known user rooms:\n"
        for a_room in self.rooms:
            msg += str(a_room) + ": " + self.rooms[a_room] + "\n"
        return msg

    def get_settings(self):
        self.logger.info("save settings")
        settings = super().get_settings()
        settings["rooms"] = self.rooms
        settings["jitsi_host"] = self.jitsi_host
        return settings

    async def get_groups(self, bot, room):
        members = list(room.users.keys())
        self.logger.info("total people in room: " + str(room.member_count) + " list: " + str(members))
        for member in members:
            if member == bot.matrix_user:
                self.logger.info("remove myself (" + bot.matrix_user + ")...i am not yet able to videochat with a person")
                members.remove(member)

        random.shuffle(members)
        self.logger.info("shuffled memberlist: " + str(members))
        return self.build_groups(members)

    def build_groups(self, members: List[str]):
        count = len(members)
        
        if count > 1:
            self.logger.info("build groups of people for " + str(count) + " members")
            half = count // 2
            split_a = members[half:]
            split_b = members[:half]

            groups = list(itertools.zip_longest(split_a, split_b))

            if count % 2 == 1:
                first_group = list(groups[0])
                lonely_warrior = groups.pop()

                self.logger.info("put lonely warrior" + str(lonely_warrior) + " into first group" + str(list(first_group)))
                first_group.append(lonely_warrior[0])
                groups[0] = tuple(first_group)

            self.logger.info("build groups of members: " + str(groups))
            return groups
        else:
            return []

    async def create_room(self, bot: Bot, user: str):
        resp = await bot.client.room_create(name="reflect-pair-¯\\_(ツ)_/¯",
                                            invite=[user],
                                            is_direct=True,
                                            preset=RoomPreset.private_chat)

        if isinstance(resp, RoomCreateResponse):
            return resp.room_id
        else:
            resp: RoomCreateError
            self.logger.info("Raum konnte nicht angelegt werden: " + resp.message)
            return None

    def help(self):
        return "build pairs of users in the current room and connect them via jitsi. Version: " + self.version

    async def send_jitsi_links(self, bot, room, groups: List[Tuple[str]]):
        self.logger.info("send jitsi link to " + str(len(groups)) + " groups: " + groups.__str__())
        if len(groups) == 0:
            await bot.send_text(room, "Ja schade. Leider sind wir alleine im Raum und ich kann das noch nicht mit Reden und so. Sorry.")
            return

        for group in groups:
            random_room_name = ''.join((random.choice(string.ascii_lowercase) for x in range(16)))
            jitsi_link = self.jitsi_host + "/" + random_room_name
            await self.send_jitsi_link(bot, room, group, jitsi_link)

        await bot.send_text(room, "Party Time. Alle hier im Raum sollten nun von mir ihren Jitsi Link gesendet bekommen haben. Wenn ihr fertig seid dann hebt eure Hand ✋")

    async def send_jitsi_link(self, bot, room, group: Tuple[str], link: str):
        old_room_id = room.room_id
        for member in group:
            if self.rooms.get(member):
                room_id = self.rooms.get(member)
            else:
                self.logger.info("create room to talk to " + member)
                room_id = await self.create_room(bot, member)
                self.rooms[member] = room_id
                bot.save_settings()

            if room_id is not None:
                room.room_id = room_id
                self.logger.info("send link " + link + " to user " + member + " in room " + room_id)
                await bot.send_text(room, "Ok. Es kann los gehen -> " + link + " Ihr seid eine Gruppe von " + str(len(group)) + " entitäten. Viel Spaß!")
            else:
                room.room_id = old_room_id
                await bot.send_text(room, "Da ist irgendwas schief gegangen. Sollte mal wer in mein Log schauen.")

        room.room_id = old_room_id
        
    def room_left(self, room: MatrixRoom):
        super().room_left(room)
        key = self.find_key_by_room(room)
        if key is not None:
            self.logger.info("remove known room " + room.room_id + " used for user " + key)
            self.rooms.pop(key)

    def find_key_by_room(self, room):
        for key in self.rooms.keys():
            if self.rooms.get(key) == room.room_id:
                return key
        return None
